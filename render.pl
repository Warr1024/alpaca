#!/usr/bin/perl -w
use strict;
use warnings;
use Data::Dumper;
use File::Find qw(find);
use File::Path qw(mkpath);
use File::Temp qw();
use Image::Imlib2 qw();
use Scalar::Util qw(looks_like_number);
use XML::Twig qw();

select STDERR;
$| = 1;

$ENV{"PATH"} = "$ENV{HOME}/bin:/bin:/usr/bin:/sbin:/usr/sbin:"
  . "/usr/local/bin:/usr/local/sbin:$ENV{PATH}";

# ----------------------------------------------------------------------
# CONFIGURATION

my $imgw = 1280;
my $imgh = $imgw * 9 / 16;

my $srcpath = shift(@ARGV) or die("usage");
my $outpath = shift(@ARGV) or die("usage");

my $cache = "tmp";
mkpath($cache);
-d $cache or die("mkpath $cache failed");

# ----------------------------------------------------------------------
# UTILITY

# Convert a string into an identifier.
sub mkid {
	my $x = lc("@_");
	$x =~ tr#A-Za-z0-9##cd;
	$x;
}

# Replace an element's display style.
sub setdisp {
	my($e, $d) = @_;
	my $st = $e->att("style") // "";
	$st =~ s#display:[^;]+#display:$d# or $st = "display:$d;$st";
	$e->set_att("style", $st);
}

# Run system command, require zero return.
sub mysys {
	my $r = system(@_);
	$r and die("system(@_) returned $r");
}

########################################################################
# LOAD AND RENDER

my @db;

sub render {
	my($svg, $outpng, $outtxt) = @_;

	my $tmpdir = File::Temp->newdir();
	my $tmpd = $tmpdir->dirname();

	# Remove old metadata file, if present, so that if we have some failure
	# after we've written the png, but before we've finalized the output
	# metadata text, the next pass will detect it as a cache miss.
	unlink($outtxt);

	# Write modified svg out to temp dir.
	open(my $fh, ">", "$tmpd/x.svg") or die($!);
	$svg->print($fh);
	close($fh);

	# Render modified svg to png.
	mysys(qw(inkscape -z -C), -w => $imgw, -h => $imgh,
	  -o => "$tmpd/raw.png", "$tmpd/x.svg");

	# Trim transparent background from image to shrink total size, but
	# add 1px border around to leave room for transparency "halo" (below).
	# Get the x/y offset coordinates.  Add a 1px transparent border BEFORE
	# trimming, too, to force transparent only to be treated as background.
	open(   $fh, "-|", "convert", "$tmpd/raw.png", qw(-bordercolor),
	  "#00000000", qw(-border 1 -trim -border 1 -print), "%X %Y", "$tmpd/trim.png"
	) or die($!);
	my($ox, $oy);
	while(<$fh>) {
		chomp;
		m#^\s*\+?(-?\d+)\s+\+?(-?\d+)\s*$# and ($ox, $oy) = ($1, $2);
	}
	close($fh);
	-s "$tmpd/trim.png" or die("trim.png not found");
	$ox // die("offset x not found");
	$oy // die("offset y not found");

	# Shift, since offset is apparently offset is relative to expanded
	# page with border, not original, now.
	$ox--; $oy--;

	# Create a near-transparent "halo" around opaque boundaries where
	# RGB values would otherwise be stripped by optimizer, so that
	# image still looks okay after linear blending and compositing.
	my $img = Image::Imlib2->load("$tmpd/trim.png");
	my($iw, $ih) = ($img->width(), $img->height());
	my $alltrans = 1;
	my @id;
	for(my $y = 0; $y < $ih; $y++) {
		for(my $x = 0; $x < $iw; $x++) {
			my($r, $g, $b, $a) = $img->query_pixel($x, $y);
			if($a) {
				undef($alltrans);
				push @id, $b, $g, $r, $a;
					next;
			}
			($r, $g, $b, $a) = (0, 0, 0, 0);
			my $t = 0;
			for(my $dy = $y ? ($y - 1) : 0; $dy < $ih and $dy <= $y + 1; $dy++) {
				for(my $dx = $x ? ($x - 1) : 0; $dx < $iw and $dx <= $x + 1; $dx++) {
					my($dr, $dg, $db, $da) = $img->query_pixel($dx, $dy);
					if($da) {
						$t += $da;
						$r += $da * $dr;
						$g += $da * $dg;
						$b += $da * $db;
					}
				}
			}
			$t
			  ? (push @id, int($b / $t), int($g / $t), int($r / $t), 1)
			  : push(@id, 0, 0, 0, 0);
		}
	}
	$alltrans and die("no pixels in $outpng");
	$img = pack("C*", @id);
	$img = Image::Imlib2->new_using_data($iw, $ih, $img);
	$img->image_set_format("png");
	$img->save($outpng);

	# Write metadata text file.
	open($fh, ">", "$outtxt.new") or die($!);
	print $fh Dumper({x => $ox, y => $oy, w => $iw, h => $ih});
	close($fh);
	rename("$outtxt.new", $outtxt);
}

my %seen;
sub proclayer {
	my($svg, $sname, $lname, $data) = @_;

	# Make sure there are no duplicate layer names.
	$seen{$sname}{$lname} and die("duplicate layer $sname.$lname");
	$seen{$sname}{$lname} = 1;

	# Calculate cached output names.
	my $n = "$sname-$lname";
	my $tmppng = "$cache/$n.png";
	my $tmptxt = "$cache/$n.txt";

	# If cache miss, render.
	-s $tmppng and -s $tmptxt or render($svg, $tmppng, $tmptxt);

	# Link image from cache to output.
	unlink("$outpath/scene-$n.png");
	link($tmppng, "$outpath/scene_${sname}_${lname}.png");

	# Load rendered metadata, add image ID to it,
	# and add it to the final data.
	my $d = do "./$tmptxt" or die("load $tmptxt failed");
	$d->{name} = $lname;
	for my $k ( keys %$data ) { $d->{$k} = $data->{$k}; }
	push @{$db[-1]->{cels} //= []}, $d;
}

sub procsvg {
	my($path, $sname) = @_;
	push @db, {name => $sname};

	# Load svg file.
	my $x = XML::Twig->new();
	$x->parsefile($path);

	# First, preprocess the document and make necessary global changes.
	my @layers;
	for my $e ($x->descendants()) {
		# Bright fuscia is our color key for special internal elements
		# like guides, and should be hidden in the final render.
		my $st = $e->att("style");
		$st and $st =~ m|#ff00ff|i and $e->tag() ne "g"
		  and $e->set_att("style", $st . ";opacity:0");

		# If there's a page background, make it transparent.
		$e->att("inkscape:pageopacity")
		  and $e->set_att("inkscape:pageopacity", "0");

		# Find layers.
		if($e->att("inkscape:groupmode") // "" eq "layer") {
			# Determine if the layer is initially visible.
			my $hid = ($e->att("style") // "") =~ m#\bdisplay\s*:\s*none\b#;

			# Add layer to the queue.  We can't process it yet, because
			# we need to finish scanning the rest of the document first.
			push @layers, [$e, $hid];

			# Hide all layers by default, so we can show them one at a time
			# during the render phase.
			setdisp($e, "none");
		}
	}

	# Render layers in a second pass, after we've finished
	# initial setup of the document.
	for my $a ( @layers ) {
		my($e, $hid) = @$a;

		# Display layer to render.
		setdisp($e, "inline");

		my $lname = $e->att("inkscape:label") // $e->att("id") // next;
		my $desc = $lname;
		$lname =~ s#\s*\|\s*(.*?)\s*$## and $desc = $1;
		$lname = mkid($lname);

		my $data = {};
		$hid or $data->{vis} = 1;
		$desc and $desc =~ m#\S# and $desc ne $lname and $data->{desc} = $desc;
		proclayer($x, $sname, $lname, $data);

		# Hide layer again, so it doesn't show when rendering next layer.
		setdisp($e, "none");
	}
}

# Find all svg files.
my @svgs;
find({          wanted => sub {
			-s $_ and m#(?:.*/)?(\d+)-(.*)\.svg$# or return;
			push @svgs, {i => $1, p => $_, n => mkid($2)};
		},
		no_chdir => 1
	},
	$srcpath);
for my $s ( sort { $a->{i} <=> $b->{i} } @svgs) {
	procsvg($s->{p}, $s->{n});
}

########################################################################

sub luadump {
	my($x, $i) = @_;
	$i //= "";
	ref $x eq "HASH" and return "{\n" . join(
		",\n",
		map {
			    "\t$i"
			  . ((m#^[A-Za-z0-9_]+$#) ? $_ : ("[" . luadump($_, "\t$i") . "]"))
			  . " = "
			  . luadump($x->{$_}, "\t$i")
		} grep { $x->{$_} } sort keys %{$x}) . "\n$i}";
	ref $x eq "ARRAY"
	  and return "{\n"
	  . join(",\n", map { "\t$i" . luadump($_, "\t$i") } @{$x}) . "\n$i}";
	looks_like_number($x) and return $x;
	$x =~ s#[\\"]#\\$1#g;
	return "\"$x\"";
}

open(my $fh, ">", "$outpath/scenedata.lua") or die($!);
print $fh "return ";
print $fh luadump({size => {w => $imgw, h => $imgh}, scenes => \@db});
close($fh);
