-- LUALOCALS < ---------------------------------------------------------
local error, ipairs, love, require
    = error, ipairs, love, require
-- LUALOCALS > ---------------------------------------------------------

local phys_state = require("phys_state")
local phys_scene = require("phys_scene")
local core_util = require("core_util")

local super = require("phys_base")
local me = super:subclass()

function me:new(init, state)
	init = super.new(self, init)
	init.state = phys_state:new(state or init.state)
	init.scenes = init.scenes or {}
	for i, v in ipairs(init.scenes) do
		v.idx = i
		v.world = init
		v.state = init.state:substate(v.name)
		v = phys_scene:new(v)
		init.scenes[i] = v
		init.scenes[v.name] = v
	end
	init:loadscript()
	for _, v in ipairs(init.scenes) do
		v:loadscript()
		if not state then v:initstate() end
	end
	if not state then init:initstate() end
	return init
end

function me:loadscript()
	if self.script ~= nil then return end
	local script = love.filesystem.load("game.lua")
	self.script = script or false
	if script then script(self) end
end

function me:tracepoint(...)
	for i = #self.scenes, 1, -1 do
		local hit = self.scenes[i]:tracepoint(...)
		if hit then return hit end
	end
end

function me:pointat(x, y)
	self.pointing = x and y and {
		x = x,
		y = y,
		trace = function()
			-- Use declaring class, not instance class,
			-- so we don't apply extra transforms from
			-- subclasses.
			return me.tracepoint(self, x, y)
		end
	} or nil
end

function me:draw()
	for _, v in ipairs(self.scenes) do
		v:draw()
	end

	-- Black border to cover antialiasing artifacts at scene boundary.
	core_util.colorize(0, 0, 0, 1, function()
			love.graphics.rectangle("line", 0, 0, self.size.w, self.size.h)
		end)

	local p = self.pointing
	if p then
		local c = p:trace()
		if c and c.desc then
			core_util.label(p.x, p.y, c.desc, self.size.w)
		end
	end
end

function me:tick(...)
	for _, v in ipairs(self.scenes) do
		v:tick(...)
	end
end

function me:act(...)
	local p = self.pointing
	if not p then return end
	p = p:trace()
	if not p then return end
	if p and p.act then return true, p, p:act(...) end
	return nil, p
end

function me:travel(name, ...)
	if name == self.state.curscene then return end
	local found = self.scenes[name] or error("scene \"" .. name .. "\" not found")
	if self.state.curscene then
		local c = self.scenes[self.state.curscene]
		if c.onleave then c:onleave(...) end
	end
	self.state.curscene = name
	if found.onenter then found:onenter(...) end
	return found
end

function me:menu() end
function me:save() end
function me:restart() end

return me
