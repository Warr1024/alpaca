local world = ...
local state = world.state

function world:initstate()
	state.global.inv = {}
	world:travel("bedroom")
end

local upact = world.act
function world:act(x, y, ...)
	local done, cb, t = upact(self, x, y, ...)
	if done then
		if t then return cb:echo(t) end
	elseif cb and cb.look then
		return cb:echo(cb.look)
	end
end
