-- LUALOCALS < ---------------------------------------------------------
local love
    = love
-- LUALOCALS > ---------------------------------------------------------

function love.conf(t)
	t.identity = "alpaca"
	t.accelerometerjoystick = false
	t.externalstorage = false

	t.window = t.window or {}
	t.window.title = "ALPACA"
	t.window.resizable = true
	t.window.fullscreen = false
	t.window.width = 1280
	t.window.height = 720

	t.modules = t.modules or {}
	t.modules.audio = false
	t.modules.joystick = false
	t.modules.physics = false
	t.modules.sound = false
	t.modules.video = false
	t.modules.thread = false
end
