-- LUALOCALS < ---------------------------------------------------------
local love, require
    = love, require
-- LUALOCALS > ---------------------------------------------------------

local super = require("phys_world")
local me = super:subclass()

function me:new(init, ...)
	init.canvas = init.canvas or love.graphics.newCanvas(init.size.w, init.size.h)
	return super.new(self, init, ...)
end

function me:drawsize() return love.graphics.getDimensions() end

function me:gettransform()
	local sw, sh = self:drawsize()
	local s = self.size

	local scalex = sw / s.w
	local scale = sh / s.h
	if scalex < scale then scale = scalex end

	local tx = (sw - s.w * scale) / 2
	local ty = (sh - s.h * scale) / 2

	return scale, tx, ty
end

function me:revtransform(x, y)
	local scale, tx, ty = self:gettransform()
	return (x - tx) / scale, (y - ty) / scale
end

function me:tracepoint(x, y)
	x, y = self:revtransform(x, y)
	return super.tracepoint(self, x, y)
end

function me:pointat(x, y)
	if x and y then
		x, y = self:revtransform(x, y)
	end
	return super.pointat(self, x, y)
end

function me:draw()
	local canvas = self.canvas
	canvas:renderTo(function() super.draw(self) end)
	local scale, tx, ty = self:gettransform()
	love.graphics.push()
	love.graphics.translate(tx, ty)
	love.graphics.scale(scale)
	love.graphics.draw(canvas)
	return love.graphics.pop()
end

return me
