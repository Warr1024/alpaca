-- LUALOCALS < ---------------------------------------------------------
local setmetatable
    = setmetatable
-- LUALOCALS > ---------------------------------------------------------

local me = {}
me.__index = me

function me:new(init)
	init = init or {}
	setmetatable(init, self)
	return init
end

function me:subclass(init)
	init = init or {}
	setmetatable(init, self)
	init.__index = init
	return init
end

return me
