-- LUALOCALS < ---------------------------------------------------------
local love, require
    = love, require
-- LUALOCALS > ---------------------------------------------------------

local core_util = require("core_util")

local super = require("phys_base")
local me = super:subclass()

me.x = 0
me.y = 0
me.h = 0
me.w = 0

function me:new(init)
	init = init or {}
	init.desc = init.desc or init.name

	local iv = init.vis
	function init.vis() return iv end

	return super.new(self, init)
end

function me:imgdata()
	return core_util.imagedata("scene_"
		.. self.scene.name
		.. "_"
		.. self.name
		.. ".png")
end
function me:img() return core_util.image(self:imgdata()) end

function me:draw()
	if not self:vis() then return end
	love.graphics.draw(self:img(), self.x, self.y)
end

local minrsqr = 16 * 16
function me:tracepoint(x, y)
	if not self:vis() then return end

	local cx = self.x + self.w / 2
	local cy = self.y + self.h / 2
	local dx = x - cx
	local dy = y - cy
	local rsqr = dx * dx + dy * dy
	if rsqr <= minrsqr then return self end

	local mx = x - self.x
	if mx < 0 or mx >= self.w then return end
	local my = y - self.y
	if my < 0 or my >= self.h then return end
	local _, _, _, a = self:imgdata():getPixel(mx, my)
	if a >= 1/16 then return self end
end

return me
