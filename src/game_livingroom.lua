-- LUALOCALS < ---------------------------------------------------------
local ipairs
    = ipairs
-- LUALOCALS > ---------------------------------------------------------

local scene = ...
local cels = scene.cels
local state = scene.state

function cels.cat:vis() return not state.cat end -- inherited global
function cels.mouse:vis() return state.cat == "asleep" end
function cels.gardendooropen:vis() return state.garden end
function cels.togarden:vis() return state.garden end
function cels.gardendoorshut:vis() return not state.garden end

-- Doors

for _, v in ipairs({"garden", "hallway"}) do
	cels["to" .. v].act = function() scene.world:travel(v) end
end

function cels.gardendooropen:act()
	state.garden = nil
end
function cels.gardendoorshut:act()
	state.garden = true
end

-- Cat interaction

function cels.cat:act()
	if state.world.sub.hallway.milk or state.world.sub.hallway.catnip then
		state.global.cat = "hall"
	end
end
