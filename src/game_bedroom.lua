local scene = ...
local state = scene.state
local cels = scene.cels

function scene:initstate()
	state.knife = true
	state.bowl = "shelf"
	state.pillow = "bed"
end

function scene:onenter()
	return state:once("intro", function()
			scene:echo([[I wake up in a strange game-like world.

				Everything is sort of low-res and half-finished. This must
				be some kind of demo or proof-of concept game. Even the
				fourth wall is missing.

				I don't have a back-story, or any coherent mission, either.

				I guess I'll just poke around and explore. This room looks
				like a bedroom; whether I'm meant to be its tenant is
				apparently ill-defined.]])
		end)
end

function cels.knife:vis() return state.knife and state.drawer end
function cels.draweropen:vis() return state.drawer end
function cels.drawershut:vis() return not state.drawer end

function cels.stool:vis() return state.stool end

function cels.pillowonbed:vis() return state.pillow == "bed" end
function cels.pillowonfloor:vis() return state.pillow == "floor" end

function cels.doorshut:vis() return not state.door end
function cels.dooropen:vis() return state.door end
function cels.tohallway:vis() return state.door end

function cels.bowlonshelf:vis() return state.bowl == "shelf" end
function cels.topshelf:vis() return state.bowl == "shelf" end
function cels.topshelfbroken:vis() return state.bowl ~= "shelf" end
function cels.bowlonpillow:vis() return state.bowl == "pillow" end
function cels.bowlbroken:vis() return state.bowl == "broken" end

cels.bed.look = "A simple twin bed, with a brass headboard, and blue sheets."
cels.sheet.look = [[Sheets of blue, to match the bed and pillow.

What's the point of making the bed, really? This is
clearly good enough...]]
cels.table.look = [[A squat, sturdy little table made of wood (or maybe
	some kind of particle board), bearing a single drawer.]]
cels.shelves.look = [[These shelves are little more than some rough planks,
resting atop brackets nailed into the wall. They don't look especially sturdy.]]
cels.topshelfbroken.look = "The top shelf is not hanging at the healthiest angle..."
cels.bowlonshelf.look = "There's a delicate glass bowl up on those shelves, out of reach."
cels.bowlbroken.look = [[The bowl is in pieces, probably never to be whole again.

This looks like one of those situations where I should probably try restarting the game.]]

-- Door to hallway.

function cels.doorshut:act()
	state.door = true
end
function cels.dooropen:act()
	state.door = nil
end
function cels.tohallway:act()
	scene.world:travel("hallway")
end

-- Place/remove the stool.

function cels.topshelf:act()
	if state.stool then
		return cels.shelves.look .. "\n\n" .. [[
		I can just barely reach the top shelf's brackets with my
		fingertips, but I can't reach any farther. Maybe if I had
		some kind of tool, I could try lowering it...]]
	else
		return cels.shelves.look .. "\n\nThe top shelf is well out of my reach."
	end
end

function cels.shelves:usestool()
	state.stool = true
	state.inv.stool = nil
	if state.bowl == "shelf" then
		return [[Placing the stool at the base of the shelves should
		allow me to reach the highest one ... well, almost, apparently.]]
	else
		return "I don't see much point to putting the stool here anymore."
	end
end
cels.bowlonshelf.usestool = cels.shelves.usestool
cels.topshelf.usestool = cels.shelves.usestool
cels.topshelfbroken.usestool = cels.shelves.usestool

function cels.stool:act()
	state.stool = nil
	state.inv.stool = true
end

-- Pillow can be picked up / placed on bed or floor.

function cels.pillowonbed:act()
	state.pillow = nil
	state.inv.pillow = true
	return state:once("pillowtook", [[I'm not sure what I can
		use this for, but I can take this pillow with me.]])
end
function cels.pillowonfloor:act()
	if state.bowl == "pillow" then
		do return [[I'd better not disturb the pillow
		while the delicate bowl rests on it...]] end
	end
	state.pillow = nil
	state.inv.pillow = true
end
function cels.bowlonshelf:usepillow()
	state.pillow = "floor"
	state.inv.pillow = nil
	return [[If I place the pillow underneath the bowl,
	I should be able to catch it, if it falls.]]
end
function cels.shelves:usepillow()
	if not cels.bowlonshelf:vis() then
		state.pillow = "floor"
		state.inv.pillow = nil
		return [[I don't see much point in putting the pillow
		here anymore, but whatever...]]
	end
	return cels.bowlonshelf:usepillow()
end
cels.topshelf.usepillow = cels.shelves.usepillow
cels.topshelfbroken.usepillow = cels.shelves.usepillow

function cels.bed:usepillow()
	state.pillow = "bed"
	state.inv.pillow = nil
	return "The bed is the pillow's rightful place."
end

-- Break the top shelf, making bowl fall either onto pillow, or shatter.

function cels.shelves:usehammer()
	return [[I can already reach the lower shelves without
	any tool assistance. It's only the top shelf that
	gives me any trouble.]]
end

function cels.topshelf:usehammer()
	if not state.stool then
		return "Good idea, but I can't reach the top shelf."
	end
	local t = [[The hammer is too clumsy of a tool to dig around
	on top of the shelf, but it's exactly the right tool
	to extract the nails holding it up.

	When the last nail is out, the top shelf suddenly
	tilts, the delicate bowl topples, and]]
	if state.pillow == "floor" then
		state.bowl = "pillow"
		return t .. " is just barely saved by the pillow."
	else
		state.bowl = "broken"
		return t .. " shatters on the floor."
	end
end
function cels.bowlonpillow:act()
	state.bowl = nil
	state.inv.bowl = true
	return "Acquiring this bowl was pretty involved. I hope it's worth it."
end

-- Bedside table

function cels.drawershut:act()
	state.drawer = true
end
function cels.draweropen:act()
	state.drawer = nil
end
function cels.knife:act()
	state.knife = nil
	state.inv.knife = true
	return "There's a small pen-knife in this drawer. I'll take it with me."
end
