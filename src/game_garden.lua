local scene = ...
local cels = scene.cels
local state = scene.state

function scene:initstate()
	state.door = true
end

cels.nepeta.look = "A small, flowering plant from the nepeta genus."

function cels.doorshut:vis() return not state.door end
function cels.dooropen:vis() return state.door end
function cels.enter:vis() return state.door end

-- Doorways

function cels.enter:act()
	scene.world:travel("livingroom")
end
function cels.doorshut:act()
	state.door = true
end
function cels.dooropen:act()
	state.door = nil
end

function cels.nepeta:useknife()
	if not state.catnip then
		state.catnip = true
		state.inv.catnip = true
	end
end
