-- LUALOCALS < ---------------------------------------------------------
local ipairs, love, require
    = ipairs, love, require
-- LUALOCALS > ---------------------------------------------------------

local phys_cel = require("phys_cel")

local super = require("phys_base")
local me = super:subclass()

function me:new(init)
	init = init or {}
	init.state = init.state or {}
	init.cels = init.cels or {}
	init = super.new(self, init)
	for i, v in ipairs(init.cels) do
		v.idx = i
		v.scene = init
		v.world = init.world
		v = phys_cel:new(v)
		init.cels[i] = v
		init.cels[v.name] = v
	end
	return init
end

function me:loadscript()
	if self.script ~= nil then return end
	local script = love.filesystem.load("game_" .. self.name .. ".lua")
	self.script = script or false
	if script then script(self) end
end

function me:vis()
	return self.state.world.curscene == self.name
end

function me:draw()
	if not self:vis() then return end
	for _, v in ipairs(self.cels) do
		v:draw()
	end
end

function me:tracepoint(...)
	if not self:vis() then return end
	for i = #self.cels, 1, -1 do
		local hit = self.cels[i]:tracepoint(...)
		if hit then return hit end
	end
end

function me:tick(...)
	for _, v in ipairs(self.cels) do
		v:tick(...)
	end
end

return me
