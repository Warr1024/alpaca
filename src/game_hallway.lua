-- LUALOCALS < ---------------------------------------------------------
local ipairs
    = ipairs
-- LUALOCALS > ---------------------------------------------------------

local scene = ...
local state = scene.state
local cels = scene.cels

function cels.cat:vis() return state.cat == "hall" end
function cels.catasleep:vis() return state.cat == "asleep" end
function cels.bowl:vis() return state.bowl end
function cels.milk:vis() return state.milk end
function cels.catnip:vis() return state.catnip end

cels.catasleep.look = [[The cat is sleeping soundly. I better
be careful not to disturb her, if I want her to stay here...]]
cels.bowl.look = [[The bowl is sitting on the floor next to the bed.]]
cels.milk.look = [[There is a little cool milk, slowly warming to
room temperature, in the bowl.]]
cels.petbed.look = "A simple, but cozy-looking bed for a pet."

for _, v in ipairs({"bedroom", "kitchen", "livingroom"}) do
	cels["to" .. v].act = function() scene.world:travel(v) end
end

-- Place things to attract cat.

function cels.petbed:usebowl()
	state.bowl = true
	state.inv.bowl = nil
end
function cels.bowl:usemilk()
	state.milk = true
end
function cels.petbed:usecatnip()
	state.catnip = true
	state.inv.catnip = nil
end
function cels.cat:act()
	if state.catnip then
		if state.milk then
			state.milk = nil
			state.global.cat = "asleep"
			return [[After drinking the milk, the cat takes a whiff of the catnip,
			rolls around for a while in bed, and falls asleep.]]
		else
			state.global.cat = nil
			return [[The cat takes a whiff of the catnip, and rolls around on
			the bed for a while, but eventually loses interest, and wanders
			back into the living room.

			I guess, if I want the cat to stay here, I'm going to need
			something else to hold her interest.]]
		end
	else
		state.milk = nil
		state.global.cat = nil
		return [[After drinking the milk, the cat loses interest,
		and wanders back the way she came.

		I guess, if I want the cat to stay here, I'm going to need
		something else to hold her interest.]]
	end
end
