-- LUALOCALS < ---------------------------------------------------------
local love, require
    = love, require
-- LUALOCALS > ---------------------------------------------------------

local core_util = require("core_util")

local super = require("phys_cel")
local me = super:subclass()

function me:text() end

function me:fontsize()
	return self.world.size.h / 20
end

function me:unwrap(s)
	return core_util.unwrap(s)
end

function me:draw()
	local t = self:text()
	if t then
		t = self:unwrap(t)
		love.graphics.setFont(core_util.fitfont(
				t, 1, self:fontsize(), self.w, self.h))
		core_util.shadowf(t, self.x, self.y, self.w)
	end
end

function me:tracepoint() end

return me
