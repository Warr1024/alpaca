-- LUALOCALS < ---------------------------------------------------------
local error, love, pcall, print, require
    = error, love, pcall, print, require
-- LUALOCALS > ---------------------------------------------------------

local core_util = require("core_util")
local core_serpack = require("core_serpack")
local phys_world_scaled = require("phys_world_scaled")

love.window.setFullscreen(not core_util.ismobile())

local prevstate
local function loadstate()
	local ok, state = pcall(function()
			local str = love.filesystem.read("autosave")
			prevstate = str
			return core_serpack.unpack(str)
		end)
	if not ok then print(state) end
	return ok and state or nil
end
local function savestate(state)
	local newstate = core_serpack.pack(state)
	if newstate ~= prevstate then
		love.filesystem.write("autosave", newstate)
		prevstate = newstate
	end
end

local scenedata = love.filesystem.load("scenedata.lua")
or error("scenedata not found; game is not properly compiled.")
local world
local function startgame(state)
	world = phys_world_scaled:new(scenedata(), state)
	world.restart = function() startgame() end
	world.save = function() savestate(world.state) end
end

startgame(loadstate())

function love.draw()
	world:draw()

	local x, y = love.graphics.getDimensions()
	local f = core_util.font(10)
	love.graphics.setFont(f)
	local t = love.timer.getFPS()
	x = x - f:getWidth(t)
	y = y - f:getHeight()
	core_util.colorize(0, 0, 0, 1/4, function()
			love.graphics.print(t, x, y)
			love.graphics.setColor(1, 1, 1, 1/4)
			return love.graphics.print(t, x - 1, y - 1)
		end)
end

function love.update(dt) world:tick(dt) end

function love.mousefocus(t) if not t then world:pointat() end end
function love.mousemoved(x, y) world:pointat(x, y) end
function love.mousereleased(_, _, _, t)
	world:act()
	world:save()
	if t then world:pointat() end
end

function love.keypressed(key)
	if key == "q" or key == "escape" then
		world:menu()
		world:save()
	elseif key == "f11" and not core_util.ismobile() then
		love.window.setFullscreen(not love.window.getFullscreen())
	end
end
