-- LUALOCALS < ---------------------------------------------------------
local error, pairs, string, table, tonumber, tostring, type
    = error, pairs, string, table, tonumber, tostring, type
local string_len, string_sub, table_concat
    = string.len, string.sub, table.concat
-- LUALOCALS > ---------------------------------------------------------

-- Encoding format characters.
local T_NUM, T_STR_S, T_STR_L, T_BACKREF, T_TBL, T_END, T_BOOL_Y, T_BOOL_N, T_NIL
= "#", "$", "&", "<", "{", "}", "+", "-", "!"

-- Short length encoding values, should fit all numeric literals.
local LENSTR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
local lengths = {}
for i = 1, #LENSTR do
	lengths[i - 1] = LENSTR:sub(i, i)
	lengths[LENSTR:sub(i, i)] = i - 1
end

------------------------------------------------------------------------
-- ENCODING

local encoders = {}
local function encode(obj, out, state)
	local enc = encoders[type(obj)]
	if not enc then return error("no encoder found for type " .. type(obj)) end
	return enc(obj, out, state)
end
local function writenum(num, out)
	local n = tostring(num)
	out(lengths[string_len(n)])
	return out(n)
end
encoders["nil"] = function(_, out)
	return out(T_NIL)
end
function encoders.number(obj, out)
	out(T_NUM)
	return writenum(obj, out)
end
function encoders.string(obj, out)
	local l = string_len(obj)
	local s = lengths[l]
	if s then
		out(T_STR_S)
		out(s)
		return out(obj)
	end
	out(T_STR_L)
	writenum(string_len(obj), out)
	return out(obj)
end
function encoders.boolean(obj, out)
	return out(obj and T_BOOL_Y or T_BOOL_N)
end
function encoders.table(obj, out, state)
	local found = state[obj]
	if found then
		out(T_BACKREF)
		return writenum(state.id - found, out)
	else
		local id = state.id + 1
		state.id = id
		state[obj] = id
	end
	out(T_TBL)
	for k, v in pairs(obj) do
		encode(k, out, state)
		encode(v, out, state)
	end
	return out(T_END)
end
local function serpack(obj)
	local t = {}
	encode(obj, function(s) t[#t + 1] = s end, {id = 1})
	return table_concat(t)
end

------------------------------------------------------------------------
-- DECODING

local decoders = {}
local function decode(str, read, key, state)
	local dec = decoders[key]
	if not dec then return error("failed to find decoder for type " .. key) end
	return dec(str, read, state)
end
decoders[T_NIL] = function() return nil end
decoders[T_BOOL_Y] = function() return true end
decoders[T_BOOL_N] = function() return false end
local function readnum(_, read)
	local l = lengths[read(1)]
	return tonumber(read(l))
end
decoders[T_NUM] = readnum
decoders[T_STR_S] = function(_, read)
	local l = lengths[read(1)]
	return read(l)
end
decoders[T_STR_L] = function(str, read)
	return read(readnum(str, read))
end
decoders[T_BACKREF] = function(str, read, state)
	local id = readnum(str, read)
	return state[state.id - id]
end
decoders[T_TBL] = function(str, read, state)
	local id = state.id + 1
	state.id = id
	local t = {}
	state[id] = t
	local k, v
	while true do
		local key = read(1)
		if key == T_END then return t end
		k = decode(str, read, key, state)
		v = decode(str, read, read(1), state)
		t[k] = v
	end
end
local function serunpack(str)
	local pos = 1
	local function read(n)
		pos = pos + n
		return string_sub(str, pos - n, pos - 1)
	end
	return decode(str, read, read(1), {id = 1})
end

------------------------------------------------------------------------

return {
	["pack"] = serpack,
	["unpack"] = serunpack
}
