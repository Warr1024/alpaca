-- LUALOCALS < ---------------------------------------------------------
local require, setmetatable, type
    = require, setmetatable, type
-- LUALOCALS > ---------------------------------------------------------

local super = require("core_class")
local me = super:subclass()

function me:new(init)
	init = init or {}
	init.global = init.global or {}

	local defaults = self:subclass()
	defaults.defaults = defaults -- Not saved in state file.
	defaults.global = init.global -- Saved in state file.
	defaults.world = init -- Pointer to root world-wide state.

	setmetatable(init, {__index = init.global})
	setmetatable(init.global, {__index = defaults})

	return init
end

function me:substate(name)
	local subs = self.sub
	if not subs then
		subs = {}
		self.sub = subs
	end
	local s = subs[name]
	if not s then
		s = {}
		subs[name] = s
	end
	setmetatable(s, {__index = self.global})
	return s
end

function me:once(flag, val, ...)
	if self[flag] then return end
	self[flag] = true
	if val == nil then return true end
	if type(val) == "function" then
		return val(...)
	else
		return val
	end
end

return me
