-- LUALOCALS < ---------------------------------------------------------
local pairs, require, tostring, type
    = pairs, require, tostring, type
-- LUALOCALS > ---------------------------------------------------------

local scene = ...
local cels = scene.cels
local world = scene.world
local state = scene.state

local core_util = require("core_util")
local phys_base = require("phys_base")
local phys_text = require("phys_text")

local function echo(cb, text)
	cb = cb and {scene = cb.scene and cb.scene.name, cel = cb.name} or nil
	state.guicb = cb and cb.scene and cb.cel and cb or nil
	state.guitext = text and tostring(text)
end
phys_base.echo = echo

phys_text:new(cels.text)
function cels.text.text() return state.guitext end

function scene.vis() return state.guicb or state.guitext end

-- Pause world while gui is open
local worldtick = world.tick
function world:tick(...)
	if not scene:vis() then return worldtick(self, ...) end
end

function cels.dismiss.act()
	state.guicb = nil
	state.guitext = nil
end

local function cblookup(cb)
	if not cb or not cb.scene or not cb.cel then return end
	local s = world.scenes[cb.scene]
	return s and s.cels[cb.cel]
end

for k, v in pairs(cels) do
	if type(k) == "string" and k:sub(1, 3) == "use" then
		local invname = k:sub(4)
		function v.vis()
			if not state.inv[invname] then return end
			local cb = cblookup(state.guicb)
			if cb and cb.scene:vis() and cb:vis()
			and not cb.noinv then
				if cb[k] then return 1 end
				return 0.5
			end
		end
		local vdraw = v.draw
		function v.draw(_, ...)
			local a = v:vis()
			if a then
				core_util.colorize(a, a, a, 1,
					function(...)
						vdraw(v, ...)
					end,
					...)
			end
		end
		function v.act()
			local cb = cblookup(state.guicb)
			if cb and cb[k] then
				local t = cb[k](cb, v)
				if t then
					return echo(cb, t)
				else
					return echo()
				end
			else
				return echo(cb, "That doesn't work here...")
			end
		end
	end
end
