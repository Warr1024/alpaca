-- LUALOCALS < ---------------------------------------------------------
local math, require
    = math, require
local math_exp
    = math.exp
-- LUALOCALS > ---------------------------------------------------------

local scene = ...
local state = scene.state
local cels = scene.cels

local core_util = require("core_util")

function scene:initstate()
	state.milk = true
	state.hammer = true
	state.stool = true
end

function cels.stool:vis() return state.stool end

function cels.milk:vis() return state.fridge and state.milk end
function cels.fridgedoor:vis() return state.fridge end
function cels.insidefridge:vis() return state.fridge end

function cels.leftcabopendoor:vis() return state.lcab end
function cels.leftcabopen:vis() return state.lcab end
function cels.leftcabclosed:vis() return not state.lcab end

function cels.hammer:vis() return state.rcab and state.hammer end
function cels.rightcabopendoor:vis() return state.rcab end
function cels.rightcabopen:vis() return state.rcab end
function cels.rightcabclosed:vis() return not state.rcab end

cels.insidefridge.look = [[You'd think a person might keep more than just
milk in the fridge, but in this case, you'd be wrong.]]
cels.leftcabopen.look = "No hidden secrets in here."
cels.rightcabopen.look = cels.leftcabopen.look
cels.sink.look = [[The knobs on this sink are stuck fast.

It's almost as if it was built by some game designer who
was too lazy to draw flowing water...]]
cels.window.look = "It looks like a nice day outside, today."
cels.counter.look = "Quartz countertops. Fancy."

-- Door to hallway.

cels.tohallway.noinv = true
function cels.tohallway:act()
	if state.stovehot then
		return "No, wait, I think I left the stove on!"
	end
	scene.world:travel("hallway")
end

-- Stool

function cels.stool:act()
	state.inv.stool = true
	state.stool = nil
	return "This stool might come in handy."
end
function cels.counter:usestool()
	state.inv.stool = nil
	state.stool = true
	return "The stool goes back from whence it came."
end

-- Refrigerator

function cels.fridge:act()
	state.fridge = not state.fridge
end
function cels.fridgedoor:act()
	state.fridge = nil
end
function cels.milk:act()
	state.milk = nil
	state.inv.milk = true
end
function cels.insidefridge:usemilk()
	state.milk = true
	state.inv.milk = nil
	return [[If I'm done with the milk, then I suppose there's
	no point in letting it go bad as I carry it around. If
	I'm not done with the milk, then I suppose I know where
	I can find it again.]]
end

-- Cabinets

function cels.leftcabclosed:act()
	state.lcab = true
end
function cels.leftcabopendoor:act()
	state.lcab = nil
end
function cels.rightcabclosed:act()
	state.rcab = true
end
function cels.rightcabopendoor:act()
	state.rcab = nil
end
function cels.hammer:act()
	state.hammer = nil
	state.inv.hammer = true
end
function cels.rightcabopen:usehammer()
	state.hammer = true
	state.inv.hammer = nil
	return [[Well, a kitchen cabinet isn't really the best place
	to keep a hammer, but I guess it's where I found it...]]
end
function cels.leftcabopen:usehammer()
	return [[That's not the right cabinet for the hammer!

	...not that ANY kitchen cabinet is really the place
	to keep a hammer in the first place...]]
end

-- Stove

function cels.stove:act()
	state.stovehot = not state.stovehot or nil
end
local heat = 0
function cels.stovehot:tick(dt)
	local q = math_exp(-dt)
	heat = heat * q + (state.stovehot and (1 - q) or 0)
end
function cels.stovehot:vis() return true end
function cels.stovehot:tracepoint() end
local stovehotdraw = cels.stovehot.draw
function cels.stovehot:draw(...)
	core_util.colorize(1, 1, 1, heat,
		function(...)
			return stovehotdraw(cels.stovehot, ...)
		end,
		...)
end
