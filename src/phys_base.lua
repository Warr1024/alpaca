-- LUALOCALS < ---------------------------------------------------------
local require
    = require
-- LUALOCALS > ---------------------------------------------------------

local super = require("core_class")
local me = super:subclass()

function me:vis() end
function me:draw() end
function me:tracepoint() end
function me:tick() end
function me:initstate() end
function me:echo() end

return me
