-- LUALOCALS < ---------------------------------------------------------
local error, ipairs, love, pcall, require, setmetatable, string, table,
      tostring
    = error, ipairs, love, pcall, require, setmetatable, string, table,
      tostring
local string_gsub, table_concat
    = string.gsub, table.concat
-- LUALOCALS > ---------------------------------------------------------

local super = require("core_class")
local me = super:subclass()

-- Temporarily change love graphics color, but be sure
-- to set it back when finished.
function me.colorize(r, g, b, a, func, ...)
	local oldr, oldg, oldb, olda = love.graphics.getColor()
	local function helper(ok, ...)
		love.graphics.setColor(oldr, oldg, oldb, olda)
		if ok then return ... end
		return error(...)
	end
	love.graphics.setColor(r, g, b, a)
	helper(pcall(func, ...))
end

-- Generate a memoization function around a core func that
-- takes a single canonical argument, and returns a single
-- value. Translate nil to special neg value for negative
-- caching.
local function mkmemo(func)
	local neg = {}
	local cache = {}
	return function(key, ...)
		local i = cache[key]
		if i ~= nil then
			if i == neg then return end
			return i
		end
		i = func(key, ...)
		cache[key] = i == nil and neg or i
		return i
	end
end

-- Memo caches for some commonly-loaded things.
me.image = mkmemo(love.graphics.newImage)
me.imagedata = mkmemo(love.image.newImageData)
me.font = mkmemo(love.graphics.newFont)

-- Create shadowed versions of both print and printf
-- as shadow and shadowf.
local function mkshadow(core)
	return function(t, x, y, ...)
		me.colorize(0, 0, 0, 1/8,
			function(...)
				for dx = -2, 2 do
					for dy = -2, 2 do
						if x ~= 0 or y ~= 0 then
							core(t, x + dx, y + dy, ...)
						end
					end
				end
				love.graphics.setColor(1, 1, 1, 1)
				return core(t, x, y, ...)
			end,
			...)
	end
end
me.shadow = mkshadow(love.graphics.print)
me.shadowf = mkshadow(love.graphics.printf)

-- Draw a floating text label, centered somewhere above a point,
-- in a standard font.
function me.label(x, y, t, w)
	local font = me.font(w / 50)
	local tw = font:getWidth(t)
	local ty = y - font:getHeight() * 3
	if ty < 0 then ty = 0 end
	local tx = x - tw / 2
	if tx < 1 then tx = 1 end
	local mtx = w - tw
	if tx > mtx then tx = mtx end
	love.graphics.setFont(font)
	me.shadow(t, tx, ty)
end

function me.fitfont(t, min, max, w, h)
	local function checkfit(s)
		local f = me.font(s)
		local ww, wt = f:getWrap(t, w)
		if not ww or not wt or (ww > w) or (#wt * f:getHeight() > h) then return end
		return f, s
	end
	local best = checkfit(max)
	if best then return best end
	while max >= (min + 1) do
		local s = (max + min) / 2
		local f = checkfit(s)
		if f then
			min = s
			best = f
		else
			max = s
		end
	end
	return best
end

-- Unwrap text lines within a paragraph, to prepare hand-wrapped
-- text for automatic re-wrapping.
local unwrap_cache = {}
setmetatable(unwrap_cache, {__mode = "k"})
function me.unwrap(s)
	local c = unwrap_cache[s]
	if c then return c end
	local t = {}
	s = string_gsub(tostring(s or ""), "\r", "")
	s = string_gsub(s, "(.-)\n%s*\n+", function(i)
			t[#t + 1] = i
			return ""
		end)
	t[#t + 1] = s
	for i, v in ipairs(t) do
		v = string_gsub(v, "%s+", " ")
		v = string_gsub(v, "^%s+", "")
		t[i] = string_gsub(v, "%s+$", "")
	end
	c = table_concat(t, "\n\n")
	unwrap_cache[s] = c
	return c
end

-- Determine if we're running on a mobile OS or a desktop one.
-- This may have some effect on e.g. the meaning of "full screen".
local mobileOS = {Android = true, iOS = true}
function me.ismobile()
	return mobileOS[love.system.getOS()]
end

return me
