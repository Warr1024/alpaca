-- LUALOCALS < ---------------------------------------------------------
local ipairs, love, pairs, require
    = ipairs, love, pairs, require
-- LUALOCALS > ---------------------------------------------------------

local scene = ...
local cels = scene.cels
local world = scene.world
local state = scene.state

local core_util = require("core_util")
local phys_scene = require("phys_scene")
local phys_text = require("phys_text")

local texts = {}
texts.help = [[
Use a pointing device (mouse or touchscreen) to play.

Click or tap on objects in scene to look at them. Hover mouse,
or tap and drag, to browse over objects, and locate small,
difficult-to-find items.

While looking at an object's description, actions and inventory
items may appear as floating icons or buttons; click or tap one
to use it on the object, otherwise, click or tap anywhere else
to dismiss.
]]
texts.restart = [[
All game progress (and any mistakes) will be reset!

Are you sure you want to restart from the beginning?
]]
texts.quit = [[
Leave your current game?

Your progress is always saved automatically, as you play.

If you want to start a new game, you should check out
the "Restart" option in the main menu.
]]

-- Move the Anchor into a separate scene, so its visibility
-- can be separate from this screen's.
local function setupanchor()
	local anchor = cels.anchor
	cels[cels.anchor.idx] = nil
	cels.anchor = nil
	anchor.desc = "Main Menu"
	function anchor:act() return world:menu() end
	local ancscene = phys_scene:new({
			name = "menu",
			world = world,
			cels = {anchor}
		})
	function ancscene:vis() return not state.v end
	world.scenes[#world.scenes + 1] = ancscene
end

-- Register this as an ESC / back button menu.
function world:menu() state.v = not state.v and "main" or nil end
function scene:vis()
	if setupanchor then
		setupanchor()
		setupanchor = nil
	end
	return state.v
end

-- Pause world while menu is open
local worldtick = world.tick
function world:tick(...)
	if not scene:vis() then return worldtick(self, ...) end
end

for _, v in pairs(cels) do v.desc = "" end

for _, k in ipairs({"resume", "instructions", "restart", "quit"}) do
	cels[k].vis = function() return state.v == "main" end
end
function cels.fullscreen:vis() return state.v == "main" and not core_util.ismobile() end

phys_text:new(cels.text)
function cels.text:text() return texts[state.v] end
cels.text.vis = cels.text.text

function cels.dismiss:vis() return cels.text:vis() end
function cels.dismiss:act() state.v = "main" end

function cels.confirm:vis() return cels.text:vis() and state.v ~= "help" end
function cels.confirm:act()
	if state.v == "restart" then return world:restart() end
	if state.v == "quit" then
		state.v = nil
		world:save()
		love.event.quit()
	end
end

function cels.resume:act() state.v = nil end
function cels.instructions:act() state.v = "help" end
function cels.restart:act() state.v = "restart" end
function cels.fullscreen:act() love.window.setFullscreen(not love.window.getFullscreen()) end
function cels.quit:act() state.v = "quit" end
