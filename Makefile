alpaca.love: render
	rm -f alpaca.love
	cd src && 7z a -tzip -mx=9 ../alpaca.love .

render:
	rm -f src/scene_*.png src/scenedata.lua
	perl -w render.pl scenes src

clean:
	rm -rf tmp
	rm -f alpaca.love src/scene_*.png src/scenedata.lua
